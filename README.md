# Simplon'game

Créer un composant carte qui affiche le titre et l'image d'un seul

App
  GameList
      CarteJeu (titre et image)
          (plus tard: quand on clique -> InfoJeu)

  InfoJeu

Créer un site qui :

- Affiche une liste de jeu (qui auront un titre et une image), contenue dans le fichier `src/gameList.js`. Il faudra afficher : le titre du jeu `name` et une image `background_image`.
- Affiche les détails d'un seul jeu, contenu dans le fichier `src/game.js`. Il faudra afficher le titre du jeu `name`, sa date de sortie `released`, sa note `rating` et une image `background_image`.
- Pouvoir ajouter le jeu en favoris !
- Charger les données de la liste de jeu à partir de l'API : https://api-games-11f15212d373.herokuapp.com/
- Charger les données du jeu à partir de l'API : https://api-games-11f15212d373.herokuapp.com/
- Utiliser le router pour aller sur la fiche d'un jeu à partir de la liste, en affichant le jeu correspondant
- Bonus : pouvoir modifier la note d'un jeu