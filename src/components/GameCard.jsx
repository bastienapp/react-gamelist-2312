function GameCard(props) {
  const { name, image } = props;

  return (
    <>
      <h2> {name} </h2>
      <img src={image} alt={name} />
    </>
  );
}
export default GameCard;
