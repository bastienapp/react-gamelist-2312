import axios from "axios";
import { useState, useEffect } from "react";

// affiche la liste des jeux (avec pour chacun le nom et l'image)
// qui affiche la liste, qui affiche une carte ?
import GameCard from "./GameCard.jsx";

function GameList() {
  // appeler l'API pour charger la liste des jeux
  let [allGames, setAllGames] = useState([]);

  useEffect(() => {
    axios
      .get("https://api-games-11f15212d373.herokuapp.com/api/games")
      .then((response) => {
        // un truc ici
        setAllGames(response.data);
      });
  }, []);

  // Warning: Each child in a list should have a unique "key" prop.

  // JSX
  return (
    <>
      <ul>
        {allGames.map((eachGame) => (
          <li key={eachGame.id}>
            <GameCard name={eachGame.name} image={eachGame.background_image} />
          </li>
        ))}
      </ul>
      {/* permet d'afficher la carte d'un seul  */}
    </>
  );
}

export default GameList;
