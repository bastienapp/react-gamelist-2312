import "./App.css";
import GameList from "./components/GameList";

function App() {
  return (
    <>
      <h1>Simplon'games</h1>
      <GameList />
    </>
  );
}

export default App;
